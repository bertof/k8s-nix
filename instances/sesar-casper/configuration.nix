{
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  console = { font = "Lat2-Terminus16"; keyMap = "us"; };

  networking = {
    hostName = "sesar-casper";
    firewall.enable = false;
  };

  services = {
    openssh.enable = true;
    qemuGuest.enable = true;
  };

  time.timeZone = "Europe/Rome";

  system.stateVersion = "23.11";
}
