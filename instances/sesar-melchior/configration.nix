{ pkgs, ... }: {
  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  console = { font = "Lat2-Terminus16"; keyMap = "us"; };

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = [ pkgs.vaapiVdpau ];
  };

  networking = {
    hostName = "sesar-melchior";
    firewall.enable = false;
  };

  services = {
    openssh.enable = true;
  };

  time.timeZone = "Europe/Rome";

  virtualisation.docker = { enable = true; enableNvidia = true; };

  system.stateVersion = "23.11";
}
