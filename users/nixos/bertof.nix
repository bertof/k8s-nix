{ pkgs, ... }: {
  users.users.bertof = {
    extraGroups = [
      "audio"
      "input"
      "docker"
      "libvirtd"
      "network"
      "networkmanager"
      "usb"
      "video"
      "wheel"
    ];
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAhxOjo9Ac9hVd3eOR56F6sClUMUh1m7VpcmzA18dslj bertof@odin"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO7mcf8fbMo1eXqSJeVFWaweB+JOU+67dFuf8laZKZZG bertof@thor"
    ];
    packages = [ pkgs.kitty.terminfo pkgs.zellij ];
    # shell = pkgs.zsh;
  };


  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
  };
}
