{
  description = "Minimal flake environment";

  inputs = {
    deploy-rs = { url = "github:serokell/deploy-rs"; inputs = { nixpkgs.follows = "nixpkgs"; }; };
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixos-generators = { url = "github:nix-community/nixos-generators"; inputs = { nixpkgs.follows = "nixpkgs"; }; };
    nixos-hardware.url = "github:NixOS/nixos-hardware";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    pre-commit-hooks-nix.url = "github:cachix/pre-commit-hooks.nix";
    systems.url = "github:nix-systems/default";
  };

  outputs = { self, ... }@inputs:
    let
      # commonModules = [ ];
      installerModules = [ ];
    in
    inputs.flake-parts.lib.mkFlake { inherit inputs; } {
      systems = import inputs.systems;
      imports = [
        # To import a flake module
        # 1. Add foo to inputs
        # 2. Add foo as a parameter to the outputs function
        # 3. Add here: foo.flakeModule
        inputs.pre-commit-hooks-nix.flakeModule
      ];
      perSystem = { config, pkgs, system, ... }: {
        # This sets `pkgs` to a nixpkgs with allowUnfree option set.
        _module.args.pkgs = import inputs.nixpkgs {
          inherit system;
          config.allowUnfree = true;
          overlays = [
            # inputs.deploy-rs.overlay
            # (_self: super: { deploy-rs = { inherit (pkgs) deploy-rs; inherit (super.deploy-rs) lib; }; })
          ];
        };

        devShells.default = pkgs.mkShell {
          buildInputs = [ pkgs.deploy-rs ];
          shellHook = ''
            ${config.pre-commit.installationScript}
          '';
        };

        formatter = pkgs.nixpkgs-fmt;

        packages = {
          # Installer ISO
          install-iso-x86_64 = inputs.nixos-generators.nixosGenerate { system = "x86_64-linux"; modules = installerModules; format = "install-iso"; };
          install-iso-aarch64 = inputs.nixos-generators.nixosGenerate { system = "aarch64-linux"; modules = installerModules; format = "install-iso"; };
          # Aarch64 base image
          aarch64-base-image = inputs.nixos-generators.nixosGenerate { system = "aarch64-linux"; modules = installerModules; format = "sd-aarch64"; };
          # Installer DigitalOcean
          do-image = inputs.nixos-generators.nixosGenerate { system = "x86_64-linux"; modules = installerModules; format = "do"; };
        };

        pre-commit.settings.hooks = {
          deadnix.enable = true;
          nixpkgs-fmt.enable = true;
          statix.enable = true;
        };
      };

      flake = {
        # Deploy-rs checks
        # checks = builtins.mapAttrs (_system: deployLib: deployLib.deployChecks deploy) inputs.deploy-rs.lib;
        checks = builtins.mapAttrs (_system: deployLib: deployLib.deployChecks self.deploy) inputs.deploy-rs.lib;

        deploy.nodes =
          let
            system = "x86_64-linux";
            pkgs = import inputs.nixpkgs { inherit system; };
            deployPkgs = import inputs.nixpkgs {
              inherit system;
              config.allowUnfree = true;
              overlays = [
                inputs.deploy-rs.overlay
                (_self: super: { deploy-rs = { inherit (pkgs) deploy-rs; inherit (super.deploy-rs) lib; }; })
              ];
            };
          in
          {
            sesar-melchior = {
              # hostname = "sesar-melchior.sesar.int";
              hostname = "melchior.sesar.int";
              # hostname = "172.20.28.231";
              profiles.system = {
                user = "root";
                path = deployPkgs.deploy-rs.lib.activate.nixos self.nixosConfigurations.sesar-melchior;
              };
            };
            sesar-casper = {
              hostname = "sesar-casper.sesar.int";
              # hostname = "172.20.28.240";
              profiles.system = {
                user = "root";
                path = deployPkgs.deploy-rs.lib.activate.nixos self.nixosConfigurations.sesar-casper;
              };
            };
            #   # odin = { hostname = "odin.zto"; profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos self'.nixosConfigurations.odin; }; };
            #   # loki = { hostname = "loki.zto"; profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos self'.nixosConfigurations.loki; }; };
            #   # baldur = { hostname = "baldur.bertof.net"; profiles.system = { user = "root"; path = inputs.deploy-rs.lib.x86_64-linux.activate.nixos self'.nixosConfigurations.baldur; }; };
            #   # freya = { hostname = "freya.zto"; profiles.system = { user = "root"; path = inputs.deploy-rs.lib.aarch64-linux.activate.nixos self'.nixosConfigurations.freya; }; };
          };

        nixosConfigurations =
          let
            commonModules = [
              ./users/nixos/bertof.nix
              ./modules/nixos/nix.nix
              ./modules/nixos/distributed.nix
              {
                nixpkgs.config = {
                  allowUnfree = true;
                  nvidia.acceptLicense = true;
                };
              }
            ];
          in
          {
            sesar-melchior = inputs.nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              modules = commonModules ++ [
                inputs.nixos-hardware.nixosModules.common-cpu-amd
                inputs.nixos-hardware.nixosModules.common-pc-ssd
                ./instances/sesar-melchior/hardware-configration.nix
                ./instances/sesar-melchior/configration.nix
                ./modules/nixos/nvidia.nix
              ];
            };
            sesar-casper = inputs.nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              modules = commonModules ++ [
                ./instances/sesar-casper/hardware-configuration.nix
                ./instances/sesar-casper/configuration.nix
                ./modules/nixos/kubernetes-master.nix

                # ./modules/nixos/server

                # ./instances/thor/hardware-configuration.nix
                # inputs.nixos-hardware.nixosModules.common-cpu-amd
                # inputs.nixos-hardware.nixosModules.common-pc-ssd
                # ./instances/thor/configuration.nix

                # ./modules/nixos/pro_audio.nix
                # ./modules/nixos/kdeconnect.nix

                # ./modules/nixos/hyprland.nix
                # {
                #   home-manager.users.bertof.imports = [
                #     ./modules/hm/hyprland.nix
                #     ./modules/hm/swayidle.nix
                #   ];
                # }
              ]
                # ++ homeManagerModules ++ [
                #   { home-manager.users.bertof = import ./instances/thor/hm.nix; }
                # ]
              ;
            };
          };
      };
    };
}
